package com.malik.alpariforstaff.fragment

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager

import com.malik.alpariforstaff.R
import com.malik.alpariforstaff.model.StaffModel

import butterknife.BindView
import butterknife.OnTextChanged
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Malik on 2017-09-11.
 */

open class SingleFragment : Fragment() {

    @BindView(R.id.main_form) protected lateinit var registerFormView: View
    @BindView(R.id.register_progress) protected lateinit var progressView: View

    //Show and hide progressBar
    protected fun showProgress(show: Boolean) {
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)

        registerFormView!!.visibility = if (show) View.GONE else View.VISIBLE
        registerFormView!!.animate().setDuration(shortAnimTime.toLong()).alpha((if (show) 0 else 1).toFloat())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        registerFormView!!.visibility = if (show) View.GONE else View.VISIBLE
                    }
                })

        progressView!!.visibility = if (show) View.VISIBLE else View.GONE
        progressView!!.animate().setDuration(shortAnimTime.toLong()).alpha((if (show) 1 else 0).toFloat())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        progressView!!.visibility = if (show) View.VISIBLE else View.GONE
                    }
                })
    }
}
