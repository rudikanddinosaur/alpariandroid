package com.malik.alpariforstaff.model;

import java.util.List;

/**
 * Created by Malik on 2017-09-27.
 */

public class ExtraWorkResult {

    private List<ExtraWorkDescription> extraWorkDescriptionList;

    public List<ExtraWorkDescription> getExtraWorkDescriptionList(){
        return extraWorkDescriptionList;
    }
}
