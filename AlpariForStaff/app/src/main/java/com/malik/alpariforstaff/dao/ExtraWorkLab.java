package com.malik.alpariforstaff.dao;

import android.util.Log;

import com.malik.alpariforstaff.SingleFragmentActivity;
import com.malik.alpariforstaff.fragment.WorkListFragment;
import com.malik.alpariforstaff.helpers.HelperClass;
import com.malik.alpariforstaff.model.ExtraWorkDescription;
import com.malik.alpariforstaff.model.ExtraWorkResult;
import com.malik.alpariforstaff.retrofitAPI.RetrofitAPI;
import com.malik.alpariforstaff.retrofitAPI.ServiceGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Malik on 2017-09-24.
 */

public class ExtraWorkLab {

//    private Call<ResponseBody> call = null;
//    private Call<ExtraWorkResult> call = null;
    private static ExtraWorkLab sWorkLab;
    private List<ExtraWorkDescription> extraWorkDescriptionListLab = new ArrayList<>();

    private ExtraWorkLab() {
    }

    public static ExtraWorkLab get() {
        if (sWorkLab == null) {
            sWorkLab = new ExtraWorkLab();
        }
        return sWorkLab;
    }

    public void setExtraWorks(List<ExtraWorkDescription> list){
        extraWorkDescriptionListLab = list;
    }

    public List<ExtraWorkDescription> getExtraWorks(){
        return extraWorkDescriptionListLab;
    }

    public void addExtraWork(ExtraWorkDescription work){

    }
}
