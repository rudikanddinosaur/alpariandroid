package com.malik.alpariforstaff.fragment;

import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.malik.alpariforstaff.R;
import com.malik.alpariforstaff.helpers.MD5;
import com.malik.alpariforstaff.helpers.MaskedEditText;
import com.malik.alpariforstaff.model.StaffModel;
import com.malik.alpariforstaff.retrofitAPI.ServiceGenerator;
import com.malik.alpariforstaff.retrofitAPI.RetrofitAPI;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Malik on 2017-08-02.
 */

public class RegisterFragment extends LoginRegisterFragment {

    @BindView(R.id.lastnameWrapper) TextInputLayout lastnameWrapper;
    @BindView(R.id.firstnameWrapper) TextInputLayout firstnameWrapper;
    @BindView(R.id.middlenameWrapper) TextInputLayout middlenameWrapper;
    @BindView(R.id.phonenumWrapper) TextInputLayout phonenumWrapper;
    @BindView(R.id.editTextPhonenum) MaskedEditText editTextPhonenum;

    private String firstName;
    private String lastName;
    private String middleName;
    private String phoneNumber;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, v);

        getLoginWrapper().setHint(getString(R.string.hint_login));
        getPasswordWrapper().setHint(getString(R.string.hint_password));
        lastnameWrapper.setHint(getString(R.string.hint_lastname));
        firstnameWrapper.setHint(getString(R.string.hint_firstname));
        middlenameWrapper.setHint(getString(R.string.hint_middlename));
        phonenumWrapper.setHint(getString(R.string.hint_phonenum));

        return v;
    }

    //Listeners
    @OnClick(R.id.buttonRegister)
    public void buttonRegisterListener(){
        hideKeyboard();

        if (checkFields()){
            showProgress(true);
            StaffModel staffModel = new StaffModel();
            staffModel.setLogin(getLogin());
            staffModel.setPassword(MD5.getMD5Hash(getPassword() + getLogin()));
            staffModel.setFirstName(firstName);
            staffModel.setLastName(lastName);
            staffModel.setMiddleName(middleName);
            staffModel.setPhoneNumber(phoneNumber);
            staffModel.setPhoneID(Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
            registerUser(staffModel);
        }
    }

    @OnTextChanged(R.id.editTextLastname)
    public void onTextChangedEditTextLastname(){
        lastName = lastnameWrapper.getEditText().getText().toString();
        if (lastName.trim().isEmpty()){
            lastnameWrapper.setError("Введите фамилию");
        }
        else {
            lastnameWrapper.setErrorEnabled(false);
        }

        if (lastName.length() >= 25)
            lastnameWrapper.setError("Вы не можете ввести более 25 символов.");
    }

    @OnTextChanged(R.id.editTextFirstname)
    public void onTextChangedEditTextFirstname(){
        firstName = firstnameWrapper.getEditText().getText().toString();
        if (firstName.trim().isEmpty()){
            firstnameWrapper.setError("Введите имя");
        }
        else {
            firstnameWrapper.setErrorEnabled(false);
        }

        if (firstName.length() >= 25)
            firstnameWrapper.setError("Вы не можете ввести более 25 символов.");
    }

    @OnTextChanged(R.id.editTextMiddlename)
    public void onTextChangedEditTextMiddlename(){
        middleName = middlenameWrapper.getEditText().getText().toString();
        if (middleName.trim().isEmpty()){
            middlenameWrapper.setError("Введите отчество");
        }
        else {
            middlenameWrapper.setErrorEnabled(false);
        }

        if (middleName.length() >= 25)
            middlenameWrapper.setError("Вы не можете ввести более 25 символов.");
    }

    @OnTextChanged(R.id.editTextPhonenum)
    public void onTextChangedEditTextPhonenum(){
        phoneNumber = editTextPhonenum.getText(true).toString();
        if (phoneNumber.length() < 10){
            phonenumWrapper.setError("Введите 10 цифр");
        }
        else {
            phonenumWrapper.setErrorEnabled(false);
        }
    }

    public void registerUser(StaffModel staffModel){
        //Creating object for our interface
        RetrofitAPI client = ServiceGenerator.createService(RetrofitAPI.class);
        setCall(client.registerStaff(staffModel));
        getCall().enqueue(new Callback<StaffModel>() {
            @Override
            public void onResponse(Call<StaffModel> call, Response<StaffModel> response) {
                if (!call.isCanceled()) {
                    showProgress(false);
                    if (response.code() == 201)
                        Toast.makeText(getActivity(), response.body().getFirstName(), Toast.LENGTH_SHORT).show();
                    else if (response.code() == 409) {
                        if (response.raw().header("duplicate").equals("login"))
                            Toast.makeText(getActivity(), "Пользователь с таким логином уже существует.", Toast.LENGTH_SHORT).show();
                        else if (response.raw().header("duplicate").equals("phoneID"))
                            Toast.makeText(getActivity(), "Этот телефон уже использовался для регистрации.",
                                    Toast.LENGTH_SHORT).show();
                    } else if (response.code() == 500) {
                        if (response.raw().header("errorMsg").equals("databaseError")) {
                            Toast.makeText(getActivity(), "Ошибка с подключением к базе данных. " +
                                            "Пожалуйста, попробуйте позже.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getActivity(), "На сервере произошла ошибка. Пожалуйста, попробуйте позже.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.code() == 422) {
                        Toast.makeText(getActivity(), "Пожалуйста, проверьте введенные данные.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<StaffModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    showProgress(false);
                    Toast.makeText(getActivity(), "Не удалось подключиться к серверу. Пожалуйста, попробуйте позже.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

//    public void registerUser(StaffModel staffModel){
//        //Here we will handle the http request to insert user to mysql db
//        Retrofit retrofit = Companion.getRetrofitInstance();
//
//        //Creating object for our interface
//        RetrofitAPI client = retrofit.create(RetrofitAPI.class);
//        setCall(client.registerStaff(staffModel));
//        getCall().enqueue(new Callback<StaffModel>() {
//            @Override
//            public void onResponse(Call<StaffModel> call, Response<StaffModel> response) {
//                if (!call.isCanceled()) {
//                    showProgress(false);
//                    if (response.code() == 201)
//                        Toast.makeText(getActivity(), response.body().getFirstName(), Toast.LENGTH_SHORT).show();
//                    else if (response.code() == 409) {
//                        if (response.raw().header("duplicate").equals("login"))
//                            Toast.makeText(getActivity(), "Пользователь с таким логином уже существует.", Toast.LENGTH_SHORT).show();
//                        else if (response.raw().header("duplicate").equals("phoneID"))
//                            Toast.makeText(getActivity(), "Этот телефон уже использовался для регистрации.",
//                                    Toast.LENGTH_SHORT).show();
//                    } else if (response.code() == 500) {
//                        Toast.makeText(getActivity(), "На сервере произошла ошибка. Пожалуйста, попробуйте позже.",
//                                Toast.LENGTH_SHORT).show();
//                    } else if (response.code() == 422) {
//                        Toast.makeText(getActivity(), "Пожалуйста, проверьте введенные данные.",
//                                Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<StaffModel> call, Throwable t) {
//                if (!call.isCanceled()) {
//                    showProgress(false);
//                    Toast.makeText(getActivity(), "Не удалось подключиться к серверу. Пожалуйста, попробуйте позже.",
//                            Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }

    private boolean checkFields(){
        boolean check = true;
        String login = getLoginWrapper().getEditText().getText().toString().trim();
        String password = getPasswordWrapper().getEditText().getText().toString().trim();
        String lastname = lastnameWrapper.getEditText().getText().toString().trim();
        String firstname = firstnameWrapper.getEditText().getText().toString().trim();
        String middlename = middlenameWrapper.getEditText().getText().toString().trim();
        String phonenum = editTextPhonenum.getText(true).toString();

        if (login.isEmpty()){
            getLoginWrapper().setError("Введите логин");
            getLoginWrapper().getEditText().requestFocus();
            check = false;
        }
        else if (password.isEmpty()) {
            getPasswordWrapper().setError("Введите пароль");
            getPasswordWrapper().getEditText().requestFocus();
            check = false;
        }
        else if (lastname.isEmpty()){
            lastnameWrapper.setError("Введите фамилию");
            lastnameWrapper.getEditText().requestFocus();
            check = false;
        }
        else if (firstname.isEmpty()){
            firstnameWrapper.setError("Введите имя");
            firstnameWrapper.getEditText().requestFocus();
            check = false;
        }
        else if (middlename.isEmpty()){
            middlenameWrapper.setError("Введите отчество");
            middlenameWrapper.getEditText().requestFocus();
            check = false;
        }
        else if (phonenum.isEmpty()){
            phonenumWrapper.setError("Введите номер телефона");
            phonenumWrapper.getEditText().requestFocus();
            check = false;
        }
        else if (phonenum.length() < 10){
            phonenumWrapper.setError("Введите 10 цифр");
            phonenumWrapper.getEditText().requestFocus();
            check = false;
        }

        if (check){
            getLoginWrapper().setErrorEnabled(false);
            getPasswordWrapper().setErrorEnabled(false);
            lastnameWrapper.setErrorEnabled(false);
            firstnameWrapper.setErrorEnabled(false);
            middlenameWrapper.setErrorEnabled(false);
            phonenumWrapper.setErrorEnabled(false);
            return check;
        }
        return check;
    }
}
