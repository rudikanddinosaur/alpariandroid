package com.malik.alpariforstaff.fragment

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import butterknife.BindView
import butterknife.OnTextChanged
import com.malik.alpariforstaff.R
import com.malik.alpariforstaff.model.StaffModel
import okhttp3.ResponseBody
import retrofit2.Call

/**
 * Created by Malik on 2017-09-29.
 */
open class LoginRegisterFragment : SingleFragment() {

    protected var login: String = ""
    protected var password: String = ""

    protected var call: Call<StaffModel>? = null

    @BindView(R.id.loginWrapper) protected lateinit var loginWrapper: TextInputLayout
    @BindView(R.id.passwordWrapper) protected lateinit var passwordWrapper: TextInputLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        loginWrapper!!.editText!!.requestFocus()
    }

    override fun onPause() {
        super.onPause()
        if (call != null && !call!!.isCanceled) {
            call!!.cancel()
            Log.i("call", "cancelled")
        }
        if (progressView!!.visibility == View.VISIBLE) {
            showProgress(false)
            Log.i("progress", "cancelled")
        }
    }

    //Listeners
    @OnTextChanged(R.id.editTextLogin)
    fun onTextChangedEditTextLogin() {
        login = loginWrapper!!.editText!!.text.toString()
        if (login.trim { it <= ' ' }.isEmpty()) {
            loginWrapper!!.error = "Введите логин"
        } else {
            loginWrapper!!.isErrorEnabled = false
        }

        if (login.length >= 25)
            loginWrapper!!.error = "Вы не можете ввести более 25 символов."
    }

    @OnTextChanged(R.id.editTextPassword)
    fun onTextChangedEditTextPassword() {
        password = passwordWrapper!!.editText!!.text.toString()
        if (password.trim { it <= ' ' }.isEmpty()) {
            passwordWrapper!!.error = "Введите пароль"
        } else {
            passwordWrapper!!.isErrorEnabled = false
        }

        if (password.length >= 25)
            passwordWrapper!!.error = "Вы не можете ввести более 15 символов."
    }


    //Helper methods
    //Hide keyboard
    protected fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }

    //Build alertDialog
    protected fun buildAlertDialog(title: String, message: String, resId: Int) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(title)
        builder.setPositiveButton(android.R.string.ok, null)
        builder.setIcon(resId)
        builder.setMessage(message)
        builder.show()
    }


}