package com.malik.alpariforstaff.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Malik on 2017-09-24.
 */

public class ExtraWorkDescription {

    private int workId;
//    private List<String> staffList = new ArrayList<>();
    private Timestamp startDateTime;
    private Timestamp endDateTime;
    private String city;
    private String address;
    private double longitude;
    private double latitude;
    private int requiredStaff;
    private int engagedStaff;

    public ExtraWorkDescription() {
    }

    public ExtraWorkDescription(int workId, Timestamp startDateTime, Timestamp endDateTime,
                                String city, String address, double longitude,
                                double latitude, int requiredStaff, int engagedStaff) {
        this.workId = workId;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.city = city;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.requiredStaff = requiredStaff;
        this.engagedStaff = engagedStaff;
    }

    public int getWorkId() {
        return workId;
    }

    public void setWorkId(int workId) {
        this.workId = workId;
    }

//    public List<String> getStaffList() {
//        return staffList;
//    }
//
//    public void setStaffList(List<String> staffList) {
//        this.staffList = staffList;
//    }

    public Timestamp getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Timestamp startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Timestamp getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Timestamp endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getRequiredStaff() {
        return requiredStaff;
    }

    public void setRequiredStaff(int requiredStaff) {
        this.requiredStaff = requiredStaff;
    }

    public int getEngagedStaff() {
        return engagedStaff;
    }

    public void setEngagedStaff(int engagedStaff) {
        this.engagedStaff = engagedStaff;
    }

}
