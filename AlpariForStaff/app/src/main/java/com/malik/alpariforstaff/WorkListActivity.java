package com.malik.alpariforstaff;

import android.support.v4.app.Fragment;

import com.malik.alpariforstaff.fragment.WorkListFragment;

/**
 * Created by Malik on 2017-09-24.
 */

public class WorkListActivity extends SingleFragmentActivity {

    private boolean mSubtitleVisible;

    // overrided methods of base activity
    @Override
    protected Fragment createFragment() {
        return new WorkListFragment();
    }


    //getters and setters
    public boolean ismSubtitleVisible() {
        return mSubtitleVisible;
    }
}
