package com.malik.alpariforstaff;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.malik.alpariforstaff.fragment.LoginFragment;
import com.malik.alpariforstaff.fragment.RegisterFragment;

public class MainActivity extends SingleFragmentActivity {

    private boolean isLogin = true;

    @Override
    protected Fragment createFragment() {
        return new LoginFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login_register_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_login_register:
                changeFragment();
                changeTitleOfMenuItem(item);
                isLogin = !isLogin;
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void changeFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (isLogin) {
            transaction.replace(R.id.fragmentContainer, new RegisterFragment());
        }
        else {
            transaction.replace(R.id.fragmentContainer, new LoginFragment());
        }
        transaction.commit();
    }

    private void changeTitleOfMenuItem(MenuItem menuItem){
        if (isLogin) {
            menuItem.setTitle(R.string.login_menuitem);
        } else {
            menuItem.setTitle(R.string.register_menuitem);
        }
    }

}
