package com.malik.alpariforstaff.model;

/**
 * Created by Malik on 2017-08-19.
 */

public class StaffModel {

    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private int positionId;
    private String phoneNumber;
    private String phoneID;
    private String token;

    public StaffModel() {
    }

    public StaffModel(String login, String password,
                      String firstName, String lastName, String middleName,
                      int positionId, String phoneNumber, String phoneID, String token) {
        super();
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.positionId = positionId;
        this.phoneNumber = phoneNumber;
        this.phoneID = phoneID;
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneID() { return phoneID; }

    public void setPhoneID(String phoneID) {
        this.phoneID = phoneID;
    }

    public String getToken() { return token; }

    public void setToken(String token) {
        this.token = token;
    }
}
