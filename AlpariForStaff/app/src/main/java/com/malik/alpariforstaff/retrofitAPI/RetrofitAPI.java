package com.malik.alpariforstaff.retrofitAPI;

import com.malik.alpariforstaff.model.ExtraWorkDescription;
import com.malik.alpariforstaff.model.ExtraWorkResult;
import com.malik.alpariforstaff.model.StaffModel;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by Malik on 2017-08-19.
 */

public interface RetrofitAPI {

    @POST("staff/login")
    Call<StaffModel> loginStaff(@Body StaffModel staffModel);

    @POST("staff/register")
    Call<StaffModel> registerStaff(@Body StaffModel staffModel);

    @GET("staff/secretinfo")
    Call<ResponseBody> getSecret();

    @GET("extraWork/getAllExtraWorks")
    Call<List<ExtraWorkDescription>> getExtraWorks(@Query("city") String city);

//    @GET("extraWork/getAllExtraWorks")
//    Call<ResponseBody> getExtraWorks(@Query("city") String city);

//    @GET("extraWork/getAllExtraWorks")
//    Call<ExtraWorkResult> getExtraWorks(@Query("city") String city);

}
