package com.malik.alpariforstaff.helpers

import com.google.gson.Gson
import com.malik.alpariforstaff.model.StaffModel

/**
 * Created by Malik on 2017-09-24.
 */
open class HelperClass {
    companion object {

        //Constants
        val TOKEN_SH_PR = "token"
        val USER_SH_PR = "user"

        fun convertToString(user : StaffModel) : String {
            var gson : Gson = Gson()
            var json : String = gson.toJson(user)
            return json
        }

        fun convertToObject(userString : String) : StaffModel{
            var gson : Gson = Gson()
            var user : StaffModel = gson.fromJson(userString, StaffModel::class.java)
            return user
        }

    }
}