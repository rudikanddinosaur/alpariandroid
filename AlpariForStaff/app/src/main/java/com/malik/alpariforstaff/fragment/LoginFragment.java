package com.malik.alpariforstaff.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.malik.alpariforstaff.R;
import com.malik.alpariforstaff.SingleFragmentActivity;
import com.malik.alpariforstaff.WorkListActivity;
import com.malik.alpariforstaff.helpers.HelperClass;
import com.malik.alpariforstaff.helpers.MD5;
import com.malik.alpariforstaff.model.StaffModel;
import com.malik.alpariforstaff.retrofitAPI.RetrofitAPI;
import com.malik.alpariforstaff.retrofitAPI.ServiceGenerator;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Malik on 2017-08-01.
 */

public class LoginFragment extends LoginRegisterFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, v);

        getLoginWrapper().setHint(getString(R.string.hint_login));
        getPasswordWrapper().setHint(getString(R.string.hint_password));
        return v;
    }

    //Listeners
    @OnClick(R.id.buttonAsk)
    public void buttonAskListener(){
        buildAlertDialog("Информация о вводимых данных", "Вы можете узнать ваши личные Логин и Пароль у вашего директора",
                android.R.drawable.ic_dialog_info);
        Log.i("phoneID", Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
    }

    @OnClick(R.id.buttonLogin)
    public void buttonLoginListener(){
        hideKeyboard();

        if (checkFields()){
            showProgress(true);
            StaffModel staffModel = new StaffModel();
            staffModel.setLogin(getLogin());
            staffModel.setPassword(MD5.getMD5Hash(getPassword() + getLogin()));
            staffModel.setPhoneID(Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
            loginUser(staffModel);
        }
    }

    public void loginUser(final StaffModel staffModel){
        //Creating object for our interface
        RetrofitAPI client = ServiceGenerator.createService(RetrofitAPI.class);
        setCall(client.loginStaff(staffModel));
        getCall().enqueue(new Callback<StaffModel>() {
            @Override
            public void onResponse(Call<StaffModel> call, Response<StaffModel> response) {
                if (!call.isCanceled()) {
                    showProgress(false);
                    if (response.isSuccessful()) {
                        Toast.makeText(getActivity(), "Здравствуй, " + response.body().getFirstName(),
                                Toast.LENGTH_SHORT).show();
                        StaffModel staffModelResponse = response.body();
                        staffModelResponse.setLogin(null);
                        staffModelResponse.setPassword(null);
                        SharedPreferences.Editor ed = SingleFragmentActivity.sPref.edit();
                        ed.putString(HelperClass.Companion.getUSER_SH_PR(), HelperClass.Companion.convertToString(staffModelResponse));
                        ed.putString(HelperClass.Companion.getTOKEN_SH_PR(), staffModelResponse.getToken());
                        ed.commit();

                        // open new activity with extraWork
                        Intent extraWorkIntent = new Intent(getActivity(), WorkListActivity.class);
                        startActivity(extraWorkIntent);
                    }
                    else if (response.code() == 401) {
                        if (response.raw().header("errorMsg").equals("loginPassword"))
                            Toast.makeText(getActivity(), "Неправильный логин или пароль", Toast.LENGTH_SHORT).show();
                        else if (response.raw().header("errorMsg").equals("phoneID"))
                            Toast.makeText(getActivity(), "Пожалуйста, войдите с устройства, " +
                                            "который вы использовали при регистрации, либо воспользуйтесь функцией смены устройства.",
                                    Toast.LENGTH_LONG).show();
                    } else if (response.code() == 500) {
                        if (response.raw().header("errorMsg").equals("databaseError")) {
                            Toast.makeText(getActivity(), "Ошибка с подключением к базе данных. " +
                                            "Пожалуйста, попробуйте позже.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getActivity(), "На сервере произошла ошибка. Пожалуйста, попробуйте позже.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if (response.code() == 422) {
                        Toast.makeText(getActivity(), "Пожалуйста, проверьте введенные данные.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<StaffModel> call, Throwable t) {
                if (!call.isCanceled()) {
                    showProgress(false);
                    Toast.makeText(getActivity(), "Не удалось подключиться к серверу. Пожалуйста, попробуйте позже.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // When we press login, this method checks our input on the client side
    private boolean checkFields(){
        boolean check = true;
        setLogin(getLoginWrapper().getEditText().getText().toString().trim());
        setPassword(getPasswordWrapper().getEditText().getText().toString().trim());

        if (getLogin().isEmpty()){
            getLoginWrapper().setError("Введите логин");
            getLoginWrapper().getEditText().requestFocus();
            check = false;
        }
        else if (getPassword().isEmpty()) {
            getPasswordWrapper().setError("Введите пароль");
            getPasswordWrapper().getEditText().requestFocus();
            check = false;
        }
        if (check){
            getLoginWrapper().setErrorEnabled(false);
            getPasswordWrapper().setErrorEnabled(false);
            return check;
        }
        return check;
    }
}
