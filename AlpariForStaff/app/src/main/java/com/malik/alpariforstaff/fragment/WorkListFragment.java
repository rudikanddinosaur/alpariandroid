package com.malik.alpariforstaff.fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.malik.alpariforstaff.R;
import com.malik.alpariforstaff.SingleFragmentActivity;
import com.malik.alpariforstaff.WorkListActivity;
import com.malik.alpariforstaff.dao.ExtraWorkLab;
import com.malik.alpariforstaff.helpers.HelperClass;
import com.malik.alpariforstaff.model.ExtraWorkDescription;
import com.malik.alpariforstaff.retrofitAPI.RetrofitAPI;
import com.malik.alpariforstaff.retrofitAPI.ServiceGenerator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Malik on 2017-09-24.
 */

public class WorkListFragment extends SingleFragment {

    private Call<List<ExtraWorkDescription>> call = null;
    private boolean mSubtitleVisible;
    private static final String SAVED_SUBTITLE_VISIBLE = "subtitle";
    @BindView(R.id.work_recycler_view) RecyclerView mWorkRecyclerView;
    private WorkAdapter mAdapter;

    // overrided methods of base fragment
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.extrawork_title);

        if (savedInstanceState != null){
            mSubtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }
        else {
            mSubtitleVisible = ((WorkListActivity) getActivity()).ismSubtitleVisible();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_SUBTITLE_VISIBLE, mSubtitleVisible);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_work_list, menu);

        MenuItem subtitleItem = menu.findItem(R.id.menu_item_show_subtitle);
        if (mSubtitleVisible) {
            subtitleItem.setTitle(R.string.hide_subtitle);
        } else {
            subtitleItem.setTitle(R.string.show_subtitle);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_work_list, container, false);
        ButterKnife.bind(this, v);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mWorkRecyclerView.setLayoutManager(layoutManager);
        mWorkRecyclerView.addItemDecoration(
                new DividerItemDecoration(getContext(), layoutManager.getOrientation()) {
                    @Override
                    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                        int position = parent.getChildAdapterPosition(view);
                        // hide the divider for the last child
                        if (position == parent.getAdapter().getItemCount() - 1) {
                            outRect.setEmpty();
                        } else {
                            super.getItemOffsets(outRect, view, parent, state);
                        }
                    }
                }
        );
//        onResume();
        return v;
    }


    //RecycleView and Adapter
    private class WorkHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mTitleTextView;
        private TextView mDateTextView;
        private TextView mStaffTextView;
        private CheckBox mSolvedCheckBox;

        private ExtraWorkDescription mExtraWorkDescription;

        public WorkHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTitleTextView = (TextView)
                    itemView.findViewById(R.id.work_list_item_titleTextView);
            mDateTextView = (TextView)
                    itemView.findViewById(R.id.work_list_item_dateTextView);
            mStaffTextView = (TextView)
                    itemView.findViewById(R.id.work_list_item_staffCount);
            mSolvedCheckBox = (CheckBox) itemView.findViewById(R.id.work_list_item_solvedCheckBox);
        }

        @Override
        public void onClick(View v) {
//            mCallbacks.onCrimeSelected(mExtraWorkDescription, mSubtitleVisible);
        }

        public void bindCrime(ExtraWorkDescription extraWorkDescription) {
            mExtraWorkDescription = extraWorkDescription;
            String title = "No title";
            if (mExtraWorkDescription.getAddress() != null && mExtraWorkDescription.getAddress().isEmpty())
                mTitleTextView.setText(title);
            else
                mTitleTextView.setText(mExtraWorkDescription.getAddress());
            mDateTextView.setText("Когда: " + formatDate("dd/MM/yyyy HH:mm", mExtraWorkDescription.getStartDateTime()) + " - " +
                    formatDate("dd/MM/yyyy HH:mm", mExtraWorkDescription.getEndDateTime()));
            mStaffTextView.setText("Сотрудники: " + mExtraWorkDescription.getEngagedStaff() +
                    "/" + mExtraWorkDescription.getRequiredStaff());
            mSolvedCheckBox.setChecked(false);
        }
    }


    private class WorkAdapter extends RecyclerView.Adapter<WorkHolder> {
        private List<ExtraWorkDescription> mExtraWorkDescriptionList;

        public WorkAdapter(List<ExtraWorkDescription> extraWorkDescriptionList) {
            mExtraWorkDescriptionList = extraWorkDescriptionList;
        }

        @Override
        public WorkHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.list_item_work, parent, false);
            return new WorkHolder(view);
        }

        @Override
        public void onBindViewHolder(WorkHolder holder, int position) {
            ExtraWorkDescription crime = mExtraWorkDescriptionList.get(position);
            holder.bindCrime(crime);
        }

        @Override
        public int getItemCount() {
            return mExtraWorkDescriptionList.size();
        }

        public void setCrimes(List<ExtraWorkDescription> extraWorkDescriptionList) {
            mExtraWorkDescriptionList = extraWorkDescriptionList;
        }
    }

    //helper methods
    private String formatDate(String format, Date date){
        DateFormat df = new DateFormat();
        String formattedDate = df.format(format, date).toString();
        return formattedDate;
    }

    private void updateUI() {
        showProgress(true);

        final List<ExtraWorkDescription> extraWorkDescriptionList = new ArrayList<>();
        RetrofitAPI client = ServiceGenerator.createService(RetrofitAPI.class, SingleFragmentActivity.sPref.getString(HelperClass.Companion.getTOKEN_SH_PR(), ""));
        call = client.getExtraWorks("Казань");
        call.enqueue(new Callback<List<ExtraWorkDescription>>() {
            @Override
            public void onResponse(Call<List<ExtraWorkDescription>> call, Response<List<ExtraWorkDescription>> response) {
                if (!call.isCanceled()) {
                    if (response.isSuccessful()) {
                        for (ExtraWorkDescription extraWork : response.body()) {
                            extraWorkDescriptionList.add(extraWork);
                        }
                        ExtraWorkLab.get().setExtraWorks(extraWorkDescriptionList);
                    } else if (response.code() == 204) {
                        Toast.makeText(getActivity(), "Список подработок пуст.", Toast.LENGTH_SHORT).show();
                        ExtraWorkLab.get().setExtraWorks(extraWorkDescriptionList);
                    } else if (response.code() == 500) {
                        Toast.makeText(getActivity(), "На сервере произошла ошибка. Пожалуйста, попробуйте позже.", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(getActivity(), "Не удалось загрузить подработки. Пожалуйста, попробуйте позже.", Toast.LENGTH_SHORT).show();
                    }

                    showProgress(false);
                    if (mAdapter == null) {
                        mAdapter = new WorkAdapter(extraWorkDescriptionList);
                        mWorkRecyclerView.setAdapter(mAdapter);
                    }
                    else {
                        mAdapter.setCrimes(extraWorkDescriptionList);
                        mAdapter.notifyDataSetChanged();
                    }
                    updateSubtitle();
                }
            }

            @Override
            public void onFailure(Call<List<ExtraWorkDescription>> call, Throwable t) {
                if (!call.isCanceled()) {
                    showProgress(false);
                    Toast.makeText(getActivity(), "Не удалось подключиться к серверу. Пожалуйста, попробуйте позже.", Toast.LENGTH_SHORT).show();
                    if (mAdapter == null) {
                        mAdapter = new WorkAdapter(extraWorkDescriptionList);
                        mWorkRecyclerView.setAdapter(mAdapter);
                    }
                    else {
                        mAdapter.setCrimes(extraWorkDescriptionList);
                        mAdapter.notifyDataSetChanged();
                    }
                    updateSubtitle();
                }
            }
        });

//        WorkListActivity workListActivity = (WorkListActivity)getActivity();
//        if (workListActivity.checkCrimeLab())
//            workListActivity.changeFragment();
//        updateSubtitle();
    }

    private void updateSubtitle() {
        int crimeCount = ExtraWorkLab.get().getExtraWorks().size();
        String subtitle = getResources()
                .getQuantityString(R.plurals.subtitle_plural, crimeCount, crimeCount);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (!mSubtitleVisible)
            subtitle = null;
        activity.getSupportActionBar().setSubtitle(subtitle);
    }

}
