package com.malik.webservice.alpari.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.malik.webservice.alpari.dao.DatabaseConn;
import com.malik.webservice.alpari.models.ExtraWorkDescriptionModel;

public class ExtraWorkService {

	public List<ExtraWorkDescriptionModel> getAllExtraWorksByCity(String cityName) throws Exception{
		List<ExtraWorkDescriptionModel> extraWorkList = new ArrayList<>();
		PreparedStatement query = null;
		Connection conn = null;
		
		try {
			conn = DatabaseConn.getDataSource().getConnection();
			query = conn.prepareStatement("SELECT ed.extraWorkDescriptionId, ed.startDateTime, ed.endDateTime, ed.requiredStaff, ed.engagedStaff, "
					+ "c.cityName, sg.address, sg.longitude, sg.latitude FROM extraWorkDescription ed "
					+ "INNER JOIN storeGeo sg ON "
					+ "ed.storeGeoId = sg.storeGeoId "
					+ "INNER JOIN cities c ON "
					+ "sg.city = c.cityId "
					+ "WHERE c.cityName = ?");
			query.setString(1, cityName);
			System.out.println(query.toString());
			ResultSet rs = query.executeQuery();
			
			while (rs.next()) {
				List<String> staffNames = new ArrayList<>();
				ExtraWorkDescriptionModel extraWork = new ExtraWorkDescriptionModel();
				extraWork.setWorkId(rs.getInt("extraWorkDescriptionId"));
				extraWork.setStartDateTime(rs.getTimestamp("startDateTime"));
				extraWork.setEndDateTime(rs.getTimestamp("endDateTime"));
				extraWork.setCity(rs.getString("cityName"));
				extraWork.setAddress(rs.getString("address"));
				extraWork.setLongitude(rs.getDouble("longitude"));
				extraWork.setLatitude(rs.getDouble("latitude"));
				extraWork.setRequiredStaff(rs.getInt("requiredStaff"));
				extraWork.setEngagedStaff(rs.getInt("engagedStaff"));
				
//				query = conn.prepareStatement("SELECT s.login FROM staff s "
//						+ "INNER JOIN extraWork ew ON "
//						+ "s.staffId = ew.staffId "
//						+ "WHERE ew.extraWorkDescriptionId = ?");
//				query.setInt(1, extraWork.getWorkId());
//				ResultSet rs2 = query.executeQuery();
//				while (rs2.next()){
//					staffNames.add(rs2.getString("login"));
//				}
//				extraWork.setStaffList(staffNames);
				extraWorkList.add(extraWork);
			}
			
			query.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		finally {
			if (conn != null)
				conn.close();
		}
		return extraWorkList;
	}
}
