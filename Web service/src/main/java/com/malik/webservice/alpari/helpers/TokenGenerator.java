package com.malik.webservice.alpari.helpers;

import java.security.SecureRandom;
import java.util.UUID;

public class TokenGenerator {

	protected static SecureRandom random = new SecureRandom();

	public static synchronized String generateToken() {
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();
		return randomUUIDString;
    }
	
	
//    public static synchronized String generateToken( String username ) {
//        long longToken = Math.abs( random.nextLong() );
//        String random = Long.toString( longToken, 16 );
//        return ( username + ":" + random );
//    }
}
