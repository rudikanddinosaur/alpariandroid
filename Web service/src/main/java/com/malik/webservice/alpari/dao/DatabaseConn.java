package com.malik.webservice.alpari.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DatabaseConn {

	private static DataSource dataSource = null;
	private static Context context = null;
	
	public static DataSource getDataSource() throws Exception {
		if (dataSource != null)
			return dataSource;
		
		try {  
			if (context == null)
				context = new InitialContext();
			
			dataSource = (DataSource)context.lookup("java:comp/env/jdbc/alpari");
			if ( dataSource == null ) {
				   throw new Exception("Data source not found!");
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return dataSource;
	}
}
