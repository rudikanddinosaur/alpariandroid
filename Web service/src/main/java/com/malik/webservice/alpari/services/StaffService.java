package com.malik.webservice.alpari.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;

import com.malik.webservice.alpari.dao.DatabaseConn;
import com.malik.webservice.alpari.helpers.TokenGenerator;
import com.malik.webservice.alpari.models.StaffModel;

public class StaffService {
	
	public boolean checkToken(String authToken) throws Exception{
		PreparedStatement query = null;
		Connection conn = null;
		boolean result = false;
		
		try {
			conn = DatabaseConn.getDataSource().getConnection();
			query = conn.prepareStatement("SELECT login FROM staff WHERE authToken = ?");
			query.setString(1, authToken);
			ResultSet rs = query.executeQuery();
			
			while (rs.next()) {
				result = true;
			}
			
			query.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (conn != null)
				conn.close();
		}
		return result;
	}
	
	private void saveToken(StaffModel staff) throws Exception {
		PreparedStatement query = null;
		Connection conn = null;
		
		try {
			conn = DatabaseConn.getDataSource().getConnection();
			query = conn.prepareStatement("UPDATE staff SET authToken = ? WHERE login = ?");
			query.setString(1, staff.getToken());
			query.setString(2, staff.getLogin());
			
			query.executeUpdate();
			
			query.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (conn != null)
				conn.close();
		}
	}
	
	public StaffModel loginStaff(StaffModel staff) throws Exception {
		PreparedStatement query = null;
		Connection conn = null;
		
		try {
			conn = DatabaseConn.getDataSource().getConnection();
			query = conn.prepareStatement("SELECT * FROM staff WHERE login = ? AND password = ?");
			query.setString(1, staff.getLogin());
			query.setString(2, staff.getPassword());
			ResultSet rs = query.executeQuery();
			
			while (rs.next()) {
				staff.setFirstName(rs.getString("firstName"));
				staff.setLastName(rs.getString("lastName"));
				staff.setMiddleName(rs.getString("middleName"));
				staff.setPositionId(rs.getInt("positionId"));
				staff.setPhoneNumber(rs.getString("phoneNumber"));
				staff.setToken(TokenGenerator.generateToken());
				saveToken(staff);
				if (staff.getPhoneID().equals(rs.getString("phoneID")))
					staff.setResult("success");
				else 
					staff.setResult("phoneID");
			}
			if (staff.getResult().equals("error"))
				staff.setResult("loginPassword");
			
			query.close();
		} catch (Exception e) {
			e.printStackTrace();
			staff.setResult("databaseError");
		}
		finally {
			if (conn != null)
				conn.close();
		}
		return staff;
	}

	public StaffModel registerStaff(StaffModel staff) throws Exception {
		PreparedStatement query = null;
		Connection conn = null;
		staff.setResult("success");
		
		try {
			conn = DatabaseConn.getDataSource().getConnection();
			query = conn.prepareStatement("INSERT INTO staff (login, password, firstName,"
					+ "lastName, middleName, positionId, phoneNumber, phoneID) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
			query.setString(1, staff.getLogin());
			query.setString(2, staff.getPassword());
			query.setString(3, staff.getFirstName());
			query.setString(4, staff.getLastName());
			query.setString(5, staff.getMiddleName());
			query.setInt(6, 1);
			query.setString(7, staff.getPhoneNumber());
			query.setString(8, staff.getPhoneID());
//			query.setString(8, "123456");
			
			query.executeUpdate();
			
			query.close(); 
		} catch (SQLIntegrityConstraintViolationException e) {
			System.out.println(e.getMessage());
			String errorMessage = e.getMessage();
			if (errorMessage.contains("login_UNIQUE"))
				staff.setResult("login");
			else if (errorMessage.contains("phoneID_UNIQUE")) 
				staff.setResult("phoneID");
			else 
				staff.setResult("inputError");
		}
		catch (Exception e) {
			e.printStackTrace();
			staff.setResult("databaseError");
		}
		finally {
			if (conn != null)
				conn.close();
		}
		return staff;
	}
}
