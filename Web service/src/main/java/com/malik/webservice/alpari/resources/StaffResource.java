package com.malik.webservice.alpari.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.persistence.internal.libraries.asm.util.CheckAnnotationAdapter;

import com.malik.webservice.alpari.dao.DatabaseConn;
import com.malik.webservice.alpari.models.StaffModel;
import com.malik.webservice.alpari.services.StaffService;

@Path("staff")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StaffResource {
	
	public static StaffService service = new StaffService();
	
	@Path("/login")
	@POST
	public Response login(StaffModel staff){
		try {
			staff = service.loginStaff(staff);
		}
		catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("ServerError")
					.build();
		}
		if (staff.getResult().equals("success")){
			return Response.status(200)
					.entity(staff)
					.build();
		}
		else if (staff.getResult().equals("phoneID")) {
			return Response.status(401)
					.header("errorMsg", "phoneID")
					.build();
		}
		else if (staff.getResult().equals("loginPassword")) {
			return Response.status(401)
					.header("errorMsg", "loginPassword")
					.build();
		}
		else if (staff.getResult().equals("databaseError")) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.header("errorMsg", "databaseError")
					.build();
		}
		return Response.status(422)
				.build();
	}
	
	@Path("/secretinfo")
	@GET
	public Response getSecretInfo(){
		return Response.status(200)
				.entity("This is a secret info")
				.build();
	}

	@Path("/register")
	@POST
	public Response register(StaffModel staff) {		
		try {
			staff = service.registerStaff(staff);
		}
		catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.build();
		}
		if (staff.getResult().equals("success")){
			return Response.status(Status.CREATED)
					.entity(staff)
					.build();
		}
		else if (staff.getResult().equals("login")) {
			return Response.status(409)
					.header("duplicate", "login")
					.build();
		}
		else if (staff.getResult().equals("phoneID")) {
			return Response.status(409)
					.header("duplicate", "phoneID")
					.build();
		}
		else if (staff.getResult().equals("inputError")) {
			return Response.status(422)
					.build();
		}
		else if (staff.getResult().equals("databaseError")) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.header("errorMsg", "databaseError")
					.build();
		}
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.build();
	}

}
