package com.malik.webservice.alpari.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.malik.webservice.alpari.models.ExtraWorkDescriptionModel;
import com.malik.webservice.alpari.services.ExtraWorkService;
import com.malik.webservice.alpari.services.StaffService;

@Path("extraWork")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ExtraWorkResource {

	public static ExtraWorkService service = new ExtraWorkService();
	
	@Path("/getAllExtraWorks")
	@GET
	public Response getAllExtraWorks(@QueryParam("city") String city){
		List<ExtraWorkDescriptionModel> extraWorkList = null;
		try {
			extraWorkList = service.getAllExtraWorksByCity(city);
		}
		catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("ServerError")
					.build();
		}
		if (extraWorkList != null && !extraWorkList.isEmpty()){
			GenericEntity<List<ExtraWorkDescriptionModel>> entity = 
					new GenericEntity<List<ExtraWorkDescriptionModel>>(extraWorkList){};
			return Response.status(200)
					.entity(entity)
					.build();
		}
		else {
			return Response.status(204)
					.header("result", "emptyList")
					.build();
		}
	}
	
}
