package com.malik.webservice.alpari.filters;

import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.internal.util.Base64;

import com.malik.webservice.alpari.resources.StaffResource;

@Provider
public class SecurityFilter implements ContainerRequestFilter{
	
	private static final String AUTHORIZATION_HEADER_KEY = "Authorization";
	private static final String AUTHORIZATION_HEADER_PREFIX = "Basic ";
	private static final String SECURED_URL_PREFIX = "secretinfo";
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		if (requestContext.getUriInfo().getPath().contains(SECURED_URL_PREFIX)){
			List<String> authHeader = requestContext.getHeaders().get(AUTHORIZATION_HEADER_KEY);
			if (authHeader!= null && authHeader.size() > 0){
				String authToken = authHeader.get(0);
				
				try {
					if (StaffResource.service.checkToken(authToken))
						return;
				} catch (Exception e) {
					e.printStackTrace();
					Response errorStatus = Response
							.status(Status.INTERNAL_SERVER_ERROR)
							.header("errorMsg", "Server error.")
							.build();
					requestContext.abortWith(errorStatus);
				}
			}
			Response unauthorizedStatus = Response
					.status(Status.UNAUTHORIZED)
					.header("errorMsg", "User cannot access the resource.")
					.build();
			requestContext.abortWith(unauthorizedStatus);
		}
	}

//	@Override
//	public void filter(ContainerRequestContext requestContext) throws IOException {
//		if (requestContext.getUriInfo().getPath().contains(SECURED_URL_PREFIX)){
//			List<String> authHeader = requestContext.getHeaders().get(AUTHORIZATION_HEADER_KEY);
//			if (authHeader!= null && authHeader.size() > 0){
//				String authToken = authHeader.get(0);
//				authToken = authToken.replaceFirst(AUTHORIZATION_HEADER_PREFIX, "");
//				String decodedString = Base64.decodeAsString(authToken);
//				StringTokenizer tokenizer = new StringTokenizer(decodedString, ":");
//				String username = tokenizer.nextToken();
//				String password = tokenizer.nextToken();
//				
//				if ("user".equals(username) && "password".equals(password))
//					return;
//			}
//			Response unauthorizedStatus = Response
//					.status(Status.UNAUTHORIZED)
//					.header("errorMsg", "User cannot access the resource.")
//					.build();
//			requestContext.abortWith(unauthorizedStatus);
//		}
//	}

}
